#!/bin/bash

set -u
source common.sh
docker kill $container_name
docker rm $container_name
docker rmi $image_name
exit 0
