#!/bin/bash

# This script allows "importing" other CppUTest project into another one
# by creating soft-links to the package directories.
#
# One can either import all packages of an existing project or specify
# a specific package to be imported as a 2nd argument to this script.
#
# The script is expected to be executed in the root directory of the project
# where other projects should be imported into.
#
# In order to use this script, the structure of the CppUTest project needs
# to be in the way it is created by the "NewProject" script. Specifically the
# include, tests and src directories must exist and the "NewPackageDirs" script
# is used to create subdirectories where code is located.

set -e
set -u

log() {
	msg="$1"
	echo "$msg"
}

# Create soft-links to package directories of another project
# in the current project.

import_package() {
	import_proj="$1"
	package="$2"
	if [ -e include/$package ]; then
		log "skipping $package - exists already" 
		return
	fi
	for subdir in include tests src; do
		ln -s ../$import_proj/$subdir/$package $subdir
	done
}

# Import packages of another project into the current one.
# NOTE The "util" directory of projects to be imported will be ignored.

import_project() {
	import_proj="$1"
	import_package=""
	if [ $# == 2 ]; then
		import_package="$2"
	fi
	for package in $(ls -1 $import_proj/include); do
		if [ $package == "util" ]; then
			continue
		fi
		# if only a specific package is to imported, skip all other ones
		if [[ $import_package != "" && $package != $import_package ]]; then
			continue
		fi
		import_package $import_proj $package
	done
}

main() {
	if [[ $# < 1 ]]; then
		echo "USAGE: $0 path-to-cpputest-project-to-import [package]"
		exit 1
	fi
	# strip optionally trailing slash from path
	import_proj="$(echo $1 | sed 's/\/$//')"
	import_package=""
	if [ $# == 2 ]; then
		import_package="$2"
	fi
	import_project "$import_proj" "$import_package"
}

main $@
