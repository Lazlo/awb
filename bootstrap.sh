#!/bin/bash

set -e
set -u

usage() {
	echo "USAGE: $0 action"
	echo ""
	echo "ACTIONS"
	echo ""
	echo -e "\tbuildslave fakeroot-dir <release> <proxy>\tcreate buildslave in fakeroot directory (run as root)"
	echo -e "\tdeps\t\t\t\t\t\tinstall target toolchain and depenencies on development host (run as root)"
	echo -e "\thost [dest-dir]\t\t\t\t\tbuild CppUTest and SimulAVR"
	echo ""
}

main() {
	if [[ $# < 1 ]]; then
		usage
		exit 1
	fi
	action="$1"
	case $action in
	"buildslave")
		if [[ $# < 2 ]]; then
			echo "ERROR: No fakeroot directory argument passed"
			exit 1
		fi
		fakeroot_dir="$2"
		if [ $UID != 0 ]; then
			echo "ERROR: Run as root"
			exit 1
		fi
		debian_release=""
		if [[ $# > 2 ]]; then
			debian_release="$3"
		fi
		apt_proxy=""
		if [[ $# > 3 ]]; then
			apt_proxy="$4"
		fi
		./bootstrap-buildslave.sh $fakeroot_dir $debian_release $apt_proxy
		;;
	"deps")
		if [ $UID != 0 ]; then
			echo "ERROR: Run as root"
			exit 1
		fi
		./bootstrap-toolchain-target-avr.sh
		./bootstrap-cpputest.sh deps
		./bootstrap-simulavr.sh deps
		;;
	"host")
		dest_dir="$(pwd)"
		if [[ $# == 3 ]]; then
			dest_dir="$3"
		fi
		./bootstrap-cpputest.sh build $dest_dir
		./bootstrap-simulavr.sh build $dest_dir
		;;
	*)
		usage
		exit 1
		;;
	esac
}
main $@
