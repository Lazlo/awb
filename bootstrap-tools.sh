#!/bin/bash

set -e
set -u

cd /root
./bootstrap-toolchain-target-avr.sh
./bootstrap-cpputest.sh deps
./bootstrap-cpputest.sh build
./bootstrap-simulavr.sh deps
./bootstrap-simulavr.sh build
