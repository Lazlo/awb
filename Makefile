default: all
all: doc

doc:
	markdown README.md > README.html
clean:
	$(RM) *.html
