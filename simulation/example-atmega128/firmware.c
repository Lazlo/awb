//
// Test firmware for the ATmega128 to be used with SimulAVR
//

#include <avr/io.h>
#include <util/delay.h>

#define DEBUG_PIN 0
#define DEBUG_PIN_DDR DDRD
#define DEBUG_PIN_PORT PORTD

#define DEBUG_PIN_AS_OUTPUT()	(DEBUG_PIN_DDR |= (1 << DEBUG_PIN))
#define DEBUG_PIN_OUT_LOW()	(DEBUG_PIN_PORT |= (1 << DEBUG_PIN))
#define DEBUG_PIN_OUT_HIGH()	(DEBUG_PIN_PORT &= ~(1 << DEBUG_PIN))
#define DEBUG_PIN_OUT_TOGGLE()	(DEBUG_PIN_PORT ^= (1 << DEBUG_PIN))

static void Task_A(void);
static void Task_B(void);
static void Task_C(void);

int main(void)
{
	// Initialize debug pin.
	DEBUG_PIN_AS_OUTPUT();

	while(1)
	{
		Task_A();
		Task_B();
		Task_C();

	}

	return 0;
}

static void Task_A(void)
{
	// Toggle debug pin.
	DEBUG_PIN_OUT_HIGH();
	_delay_ms(1);

	DEBUG_PIN_OUT_LOW();
	_delay_ms(1);
}

static void Task_B(void)
{
}

static void Task_C(void)
{
}
