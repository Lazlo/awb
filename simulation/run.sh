#
# Configuration
#

# Path to firmware ELF file.

flash_img=example-atmega128/firmware.elf

# Target device.

mcu=atmega128

# Traget clock frequency in Hz.

f_cpu=16000000


#
# Construct SimulAVR Options
#

simulavr_opts=""

# Pass target device name.
simulavr_opts+="--device ${mcu}"

# Pass target clock frequency.
simulavr_opts+=" --cpufrequency ${f_cpu}"

simulavr_opts+=" --terminate exit"

# Path to firmware ELF file.
simulavr_opts+=" --file ${flash_img}"

#simulavr_opts+=" --clock-freq ${f_cpu}"
#simulavr_opts+=" --writetopipe 0x20,-"
#simulavr_opts+=" --readfrompipe 0x22,-"

# SimulAVR binary from system package (v0.1.2.2)
simulavr_bin=simulavr

# SimulAVR binary build from source from git repo (1.1dev)
simulavr_bin=/home/lazlo/src/simulavr.git/src/simulavr

#
# Run SimulAVR
#

cmd="${simulavr_bin} ${simulavr_opts}"
echo $cmd
$cmd
