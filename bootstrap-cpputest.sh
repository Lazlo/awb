#!/bin/bash

set -e
set -u

install_deps() {
	deps="build-essential libtool autoconf automake git-core"
	apt-get -q install -y $deps
}

fetch() {
	dest_dir="$1"
	cd $dest_dir
	git clone https://github.com/cpputest/cpputest.git
	cd -
}

build() {
	dest_dir="$1"
	cd $dest_dir/cpputest
	./autogen.sh
	./configure
	make
	make tdd
	cd -
}

usage() {
	echo "USAGE: $0 deps|build"
}

main() {
	if [[ $# < 1 ]]; then
		usage
		exit 1
	fi
	action="$1"
	case $action in
	"deps")
		if [ $UID != 0 ]; then
			echo "ERROR: Run as root"
			exit 1
		fi
		install_deps
		;;
	"build")
		dest_dir="$(pwd)"
		if [[ $# == 2 ]]; then
			dest_dir="$2"
		fi
		fetch $dest_dir
		build $dest_dir
		;;
	*)
		usage
		exit 1
		;;
	esac
}

main $@
