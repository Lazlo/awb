#!/bin/bash

set -e
set -u
source common.sh
docker build --build-arg=AWB_USER="$awb_user" --build-arg=AWB_HOME="$awb_home" -t $image_name .
docker create -p $host_ssh_port:22 --name $container_name $image_name
docker start $container_name
