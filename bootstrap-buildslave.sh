#!/bin/bash

set -e
set -u

DEFAULT_DEB_MIRROR=ftp.de.debian.org

main() {
	if [ $UID != 0 ]; then
		echo "ERROR: Run as root"
		exit 1
	fi
	if [[ $# < 1 ]]; then
		echo "USAGE: $0 buildslave-root-dir release proxy"
		exit 1
	fi
	# Release
	release=`lsb_release -cs`
	if [[ $# > 1 ]]; then
		release="$2"
	fi
	basedir="$1"

	# Proxy
	proxy_address=""
	proxy_url=""
	if [[ $# == 3 ]]; then
		proxy_address="$3"
		proxy_url="http://$proxy_address"
	fi
	# Mirror
	mirror_address=$DEFAULT_DEB_MIRROR
	mirror_url=http://$mirror_address/debian

	http_proxy=$proxy_url debootstrap --verbose $release $basedir $mirror_url
	echo "Acquire::http::Proxy \"$proxy_url\";" > $basedir/etc/apt/apt.conf
	cp bootstrap-tools.sh $basedir/root
	cp bootstrap-toolchain-target-avr.sh $basedir/root
	cp bootstrap-cpputest.sh $basedir/root
	cp bootstrap-simulavr.sh $basedir/root
	chroot $basedir /root/bootstrap-tools.sh
}

main $@
