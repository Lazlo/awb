# TODO

related to CI:

 - add support for traviy.yml build config
 - add cron to pull git and build
 - notification support
 - build history support
   - track binary size on target

related to makefile:

 - "merge" CppUTest and AVR makefiles
 - support dual target build
 - support building for release, debug and testing
 - support for gcov
 - support for gprof
 - support for nemiver

support for test frameworks:

 - unity
 - cmock
 - ~~cpputest~~
   - install scripts
   - add CPPUTEST_HOME to env
   - install our ImportProject.sh script
 - gmock?

simulation:

 - ~~simulavr integration~~

user settings to tune:

 - vim scripts for CI (run tests on save)
