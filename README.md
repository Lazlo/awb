# AVR Workbench (AWB)

Standard Build Environment for developing AVR microcontroller firmware.

Aims at supporting development of C software for the AVR microcontroller
by bringing together ...

 * _test-driven development_
 * _simulation_
 * _continuous integration_.

Contains:

 * a Dockerfile to create container image with AWB installed
 * a Makefile for C for the AVR target
 * shell scripts for
   - creating a buildslave
   - installing the AVR toolchain (GCC, GDB, binutils, avr-libc, avrdude)
   - building [SimulAVR](http://www.nongnu.org/simulavr/)
   - building [CppUTest](https://cpputest.github.io/)
   - importing packages of a CppUTest project into another
   - creating a .gitignore file with patterns of common build artifacts

## Dependencies

```
sudo apt-get -q install -y debootstrap
```

## Usage

 1. clone this git repository
 2. change into the repository directory
 3. run ```sudo ./bootstrap.sh buildslave <fakeroot-dir> <optional-name-of-debian-release> <optional-proxy-address-and-port>``` to create a buildslave machine
 4. run ```sudo ./bootstrap.sh deps``` to install host depenencies. this will install
    - the target toolchain
    - tools for building CppUTest
    - tools for building SimulAVR
 5. run ```./bootstrap.sh host``` to build CppUTest and SimulAVR in one step
 6. run CppUTest script ```NewProject``` to create a new project
 7. make the new project directory a git repository
 8. create gitignore file for the new project repository

```
mkdir Workspace
cd Workspace
git clone https://gitlab.com/Lazlo/awb.git
cd awb
sudo ./awb/bootstrap.sh buildslave.sh buildslave-avr-fakeroot
sudo ./awb/bootstrap.sh deps
./awb/bootstrap.sh host
NewProject MyProject
git init MyProject
cd MyProject
../awb/gen-gitignore-for-Makefile-avr
git add .gitignore
git commit -m "Add patterns for build artifacts to .gitignore."
```
