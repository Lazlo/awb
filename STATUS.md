# Status

| Debian Release   | Outcome |
| ---------------- | ------- |
| Sid (unstable)   | breaks  |
| Buster (10)      | breaks  |
| Stretch (9)      | breaks  |
| Jessie (8)       | works   |
| Wheezy (7)       | breaks  |

## Errors

Building simulavr with Sid breaks

```
Making all in gtest
make[2]: Entering directory '/root/simulavr/regress/gtest'
depbase=`echo session_001/unittest001.o | sed 's|[^/]*$|.deps/&|;s|\.o$||'`;\
g++ -DHAVE_CONFIG_H -I. -I../../src    -Dprivate=public -Dprotected=public  -Igtest-1.6.0/include/gtest -Igtest-1.6.0/include -Igtest-1.6.0 -I../../src -g -g -O2 -MT session_001/unittest001.o -MD -MP -MF $depbase.Tpo -c -o session_001/unittest001.o session_001/unittest001.cpp &&\
mv -f $depbase.Tpo $depbase.Po
In file included from gtest-1.6.0/include/gtest/internal/gtest-port.h:197,
                 from gtest-1.6.0/include/gtest/internal/gtest-internal.h:40,
                 from gtest-1.6.0/include/gtest/gtest.h:57,
                 from session_001/unittest001.cpp:4:
/usr/include/c++/8/sstream:301:7: error: 'struct std::__cxx11::basic_stringbuf<_CharT, _Traits, _Alloc>::__xfer_bufptrs' redeclared with different access
       struct __xfer_bufptrs
       ^~~~~~
make[2]: *** [Makefile:581: session_001/unittest001.o] Error 1
make[2]: Leaving directory '/root/simulavr/regress/gtest'
make[1]: *** [Makefile:430: all-recursive] Error 1
make[1]: Leaving directory '/root/simulavr/regress'
make: *** [Makefile:507: all-recursive] Error 1
```
