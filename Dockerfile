FROM debian
ARG AWB_USER=build
ARG AWB_HOME=/home/build
RUN apt-get update > /dev/null && apt-get -q install -y openssh-server sudo > /dev/null
RUN mkdir -p /var/run/sshd
RUN useradd -d $AWB_HOME -m -s /bin/bash $AWB_USER
RUN echo echo "$AWB_USER ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/$AWB_USER
# Run as daemon and write debug logs to standard error.
CMD ["/usr/sbin/sshd", "-D", "-e"]
