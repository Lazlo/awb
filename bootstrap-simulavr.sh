#!/bin/bash

set -e
set -u

install_deps() {
	deps="build-essential libtool libtool-bin autoconf automake swig python texinfo git-core"
	apt-get -q install -y $deps
}

fetch() {
	dest_dir="$1"
	cd $dest_dir
	git clone https://git.savannah.nongnu.org/git/simulavr.git
	cd -
}

build() {
	dest_dir="$1"
	cd $dest_dir/simulavr
	./bootstrap
	./configure
	make
	cd -
}

usage() {
	echo "USAGE: $0 deps|build"
}

main() {
	if [[ $# < 1 ]]; then
		usage
		exit 1
	fi
	action="$1"
	case $action in
	"deps")
		if [ $UID != 0 ]; then
			echo "ERROR: Run as root"
			exit 1
		fi
		install_deps
		;;
	"build")
		dest_dir="$(pwd)"
		if [[ $# == 2 ]]; then
			dest_dir="$2"
		fi
		fetch $dest_dir
		build $dest_dir
		;;
	*)
		usage
		exit 1
		;;
	esac
}

main $@
