#!/bin/bash

set -e
set -u

deps="gcc-avr binutils-avr avr-libc gdb-avr avrdude"
apt-get -q install -y $deps
